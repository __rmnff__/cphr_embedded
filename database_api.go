package api

import (
	"database/sql"
	"fmt"
	"os"
	"strconv"
	"time"

	_ "github.com/mattn/go-sqlite3" // required as an extension to database/sql
)

// NewMessage defines structure for all messages inside application
type NewMessage struct {
	ID     string `json:"id"`
	Type   string `json:"type"`
	Date   string `json:"date"`
	Status string `json:"status"`
	Sender string `json:"author"`
	Text   string `json:"text"`
	Pinned string `json:"pinned"`
}

// NewUser defines user's structure
type NewUser struct {
	Link string
	Addr string
	Hash string
}

// User defines expanded user's structure
type User struct {
	Username      string       `json:"username"`
	Link          string       `json:"link"`
	Addr          string       `json:"addr"`
	LastMessage   NewMessage   `json:"lastMessage"`
	NewMessages   string       `json:"newMessages"`
	Notifications []NewMessage `json:"notifications"`
	LastOnline    string       `json:"lastOnline"`
}

// AddTableColumn allows database edit within application's API
func (c *Commander) AddTableColumn(tname string, cname string, ctype string, dvalue string, dbname string) {
	dbnames := []string{dbname}
	for i := range dbnames {
		db, err := c.openDB(dbnames[i])
		if err != nil {
			e := fmt.Sprintf("No such database with name %s found", dbnames[i])
			fmt.Println(e)
			return
		}
		defer closeDB(db)
		stmnt := fmt.Sprintf("select %s from %s limit 1;", cname, tname)
		_, err = db.Prepare(stmnt)
		if err != nil {
			stmnt = fmt.Sprintf("alter table %s add column %s %s;", tname, cname, ctype)
			_, err = db.Exec(stmnt)
			if err != nil {
				fmt.Println(err)
				fmt.Println("Can't alter table from database ", dbnames[i])
				return
			}
			stmnt = fmt.Sprintf("update %s set %s = %s", tname, cname, dvalue)
			_, err = db.Exec(stmnt)
			if err != nil {
				fmt.Println("Can't update to default value")
				return
			}
		}
	}
}

// UpdateStorage should be used everytime an app starts
func (c *Commander) UpdateStorage() bool {
	db, err := c.openDB("history")
	if err != nil {
		return false
	}
	stmnt := `create table if not exists knownUsers(
	id integer not null primary key,
	username text,
	link text,
	address text,
	hash text);`
	_, err = db.Exec(stmnt)
	if err != nil {
		closeDB(db)
		return false
	}
	closeDB(db)
	db, err = c.openDB("blocks")
	if err != nil {
		return false
	}
	stmnt = `create table if not exists blocks(
	id integer not null primary key,
	hash text,
	number int);`
	_, err = db.Exec(stmnt)
	if err != nil {
		closeDB(db)
		return false
	}
	closeDB(db)
	return true
}

func (c *Commander) openDB(name string) (*sql.DB, error) {
	path := c.ConstantPath
	fullPath := fmt.Sprintf("%s/history/%s.db", path, name)
	db, err := sql.Open("sqlite3", fullPath)
	if err != nil {
		return &sql.DB{}, err
	}
	return db, nil
}

func closeDB(db *sql.DB) bool {
	db.Close()
	return true
}

// GetLinkByAddress fetches user's Onion Link using it's ETH address
func (c *Commander) GetLinkByAddress(address string) string {
	var link string
	db, err := c.openDB("history")
	if err != nil {
		return ""
	}
	defer closeDB(db)
	stmnt := "select link from knownUsers where address = ?"
	st, err := db.Prepare(stmnt)
	if err != nil {
		return ""
	}
	defer st.Close()
	err = st.QueryRow(address).Scan(&link)
	if err != nil {
		return ""
	}
	return link
}

// GetAddressByLink fetches user's ETH address using it's Onion Link
func (c *Commander) GetAddressByLink(link string) string {
	var address string
	db, err := c.openDB("history")
	if err != nil {
		return ""
	}
	defer closeDB(db)
	stmnt := "select address from knownUsers where link = ?"
	st, err := db.Prepare(stmnt)
	if err != nil {
		return ""
	}
	defer st.Close()
	err = st.QueryRow(link).Scan(&address)
	if err != nil {
		return ""
	}
	return address
}

// GetCipherByAddress gets cipher private key for conversation using companions address
func (c *Commander) GetCipherByAddress(address string) string {
	var cipher string
	db, err := c.openDB("history")
	if err != nil {
		return ""
	}
	defer closeDB(db)
	stmnt := "select hash from knownUsers where address = ?"
	st, err := db.Prepare(stmnt)
	if err != nil {
		return ""
	}
	defer st.Close()
	err = st.QueryRow(address).Scan(&cipher)
	if err != nil {
		return ""
	}
	return cipher
}

// CheckExistance checks if user is already in contact database
func (c *Commander) CheckExistance(link string) bool {
	db, err := c.openDB("history")
	if err != nil {

		return true
	}
	defer closeDB(db)
	stmnt := "select address from knownUsers where link = ?"
	st, err := db.Prepare(stmnt)
	if err != nil {

		return true
	}
	defer st.Close()
	address := ""
	err = st.QueryRow(link).Scan(&address)
	if err != nil {
		return false
	}
	return true
}

// GetChats fetches all chats a user has
func (c *Commander) GetChats() []User {
	var users []User
	db, err := c.openDB("history")
	if err != nil {

		return []User{}
	}
	defer closeDB(db)
	stmnt := `select username, link, address from knownUsers;`
	rows, err := db.Query(stmnt)
	if err != nil {
		return []User{}
	}
	defer rows.Close()
	for rows.Next() {
		var username string
		var link string
		var address string
		err = rows.Scan(&username, &link, &address)
		if err != nil {
			return []User{}
		}
		lastMsg, err := c.GetLastMessage(address)
		if err != nil {
			return []User{}
		}
		newMsgs, err := c.GetNewMessages(address)
		if err != nil {
			return []User{}
		}
		notifications := c.GetNotifications(address)
		newMsgsStringified := strconv.Itoa(newMsgs)
		lastOnline, _ := c.GetLastOnline(address)
		users = append(
			users, User{
				username,
				link,
				address,
				lastMsg,
				newMsgsStringified,
				notifications,
				lastOnline,
			})
	}
	err = rows.Err()
	if err != nil {
		return []User{}
	}
	return users
}

// GetFullChatHistory fetches all messages based on the companion's address
func (c *Commander) GetFullChatHistory(addr string) ([]NewMessage, error) {
	var messages []NewMessage
	db, err := c.openDB(addr)
	if err != nil {
		return []NewMessage{}, err
	}
	defer closeDB(db)
	if c.OldMessagesCount < 0 {
		var amount int
		stmnt := "select coalesce(max(id)+1, 0) from messages;"
		rows, err := db.Query(stmnt)
		if err != nil {
			return []NewMessage{}, err
		}
		for rows.Next() {
			err = rows.Scan(&amount)
			if err != nil {
				fmt.Println(err)
				rows.Close()
				return []NewMessage{}, err
			}
			c.SetMessageCount(amount - 1)
			c.SetOldMessageCount(amount - 1)
		}
		err = rows.Err()
		if err != nil {
			fmt.Println(err)
			rows.Close()
			return []NewMessage{}, err
		}
		rows.Close()
	}
	stmnt := "select id, origin, date, status, sender, input, pinned from messages;"
	rows, err := db.Query(stmnt)
	if err != nil {
		fmt.Println(err)
		return []NewMessage{}, err
	}
	for rows.Next() {
		var id string
		var origin string
		var date string
		var status string
		var sender string
		var input string
		var pinned string
		err = rows.Scan(&id, &origin, &date, &status, &sender, &input, &pinned)
		if err != nil {
			fmt.Println(err)
			rows.Close()
			return []NewMessage{}, err
		}
		messages = append(messages, NewMessage{id, origin, date, status, sender, input, pinned})
	}
	err = rows.Err()
	if err != nil {
		fmt.Println(err)
		rows.Close()
		return []NewMessage{}, err
	}
	rows.Close()
	c.SetOldMessageCount(c.OldMessagesCount - 15)
	return messages, nil
}

// GetChatHistory same as GetFullChatHistory
func (c *Commander) GetChatHistory(addr string) ([]NewMessage, error) {
	if c.OldMessagesCount < 0 {
		return []NewMessage{}, nil
	}
	var messages []NewMessage
	db, err := c.openDB(addr)
	if err != nil {
		return []NewMessage{}, err
	}
	defer closeDB(db)
	if !c.OldMessagesObsrvr {
		var amount int
		stmnt := "select coalesce(max(id)+1, 0) from messages;"
		rows, err := db.Query(stmnt)
		if err != nil {
			return []NewMessage{}, err
		}
		for rows.Next() {
			err = rows.Scan(&amount)
			if err != nil {
				fmt.Println(err)
				rows.Close()
				return []NewMessage{}, err
			}
			c.SetMessageCount(amount - 1)
			c.SetOldMessageCount(amount - 1)
		}
		err = rows.Err()
		if err != nil {
			fmt.Println(err)
			rows.Close()
			return []NewMessage{}, err
		}
		rows.Close()
		c.StartObserving()
	}
	stmnt := "select id, origin, date, status, sender, input, pinned from messages limit ? offset ?;"
	amnt := 15
	offset := c.OldMessagesCount - 15
	if offset < 0 {
		amnt += offset
		offset = 0
	}
	rows, err := db.Query(stmnt, amnt, offset)
	if err != nil {
		fmt.Println(err)
		return []NewMessage{}, err
	}
	for rows.Next() {
		var id string
		var origin string
		var date string
		var status string
		var sender string
		var input string
		var pinned string
		err = rows.Scan(&id, &origin, &date, &status, &sender, &input, &pinned)
		if err != nil {
			fmt.Println(err)
			rows.Close()
			return []NewMessage{}, err
		}
		messages = append(messages, NewMessage{id, origin, date, status, sender, input, pinned})
	}
	err = rows.Err()
	if err != nil {
		fmt.Println(err)
		rows.Close()
		return []NewMessage{}, err
	}
	rows.Close()
	c.SetOldMessageCount(c.OldMessagesCount - 15)
	return messages, nil
}

// GetLatestMessages fetches last N messages based on companion's address
func (c *Commander) GetLatestMessages(addr string) ([]NewMessage, error) {
	if c.MessagesCount == 0 {
		return []NewMessage{}, nil
	}
	var messages []NewMessage
	db, err := c.openDB(addr)
	if err != nil {
		return []NewMessage{}, err
	}
	defer closeDB(db)
	stmnt := "select id, origin, date, status, sender, input, pinned from messages limit -1 offset ?;"
	rows, err := db.Query(stmnt, c.MessagesCount)
	if err != nil {
		fmt.Println(err)
		return []NewMessage{}, err
	}
	for rows.Next() {
		var id string
		var origin string
		var date string
		var status string
		var sender string
		var input string
		var pinned string
		err = rows.Scan(&id, &origin, &date, &status, &sender, &input, &pinned)
		if err != nil {
			fmt.Println(err)
			rows.Close()
			return []NewMessage{}, err
		}
		messages = append(messages, NewMessage{id, origin, date, status, sender, input, pinned})
	}
	err = rows.Err()
	if err != nil {
		fmt.Println(err)
		rows.Close()
		return []NewMessage{}, err
	}
	rows.Close()
	var amount int
	stmnt = "select coalesce(max(id)+1, 0) from messages;"
	rows, err = db.Query(stmnt)
	if err != nil {
		return []NewMessage{}, err
	}
	defer rows.Close()
	for rows.Next() {
		err = rows.Scan(&amount)
		if err != nil {
			return []NewMessage{}, err
		}
	}
	err = rows.Err()
	if err != nil {
		return []NewMessage{}, err
	}
	c.SetMessageCount(amount - 1)
	return messages, nil
}

// GetLastMessage returns very last message
// func (c *Commander) GetLastMessage(addr string) ([]NewMessage, error) {
// 	var messages []NewMessage
// 	db, err := c.openDB(addr)
// 	if err != nil {
// 		return []NewMessage{}, err
// 	}
// 	defer closeDB(db)
// 	stmnt := "select id, origin, date, status, sender, input, pinned from messages order by id desc limit -1;"
// 	rows, err := db.Query(stmnt, c.MessagesCount)
// 	if err != nil {
// 		fmt.Println(err)
// 		return []NewMessage{}, err
// 	}
// 	for rows.Next() {
// 		var id string
// 		var origin string
// 		var date string
// 		var status string
// 		var sender string
// 		var input string
// 		var pinned string
// 		err = rows.Scan(&id, &origin, &date, &status, &sender, &input, &pinned)
// 		if err != nil {
// 			fmt.Println(err)
// 			rows.Close()
// 			return []NewMessage{}, err
// 		}
// 		messages = append(messages, NewMessage{id, origin, date, status, sender, input, pinned})
// 	}
// 	err = rows.Err()
// 	if err != nil {
// 		fmt.Println(err)
// 		rows.Close()
// 		return []NewMessage{}, err
// 	}
// 	rows.Close()
// 	return messages, nil
// }

// GetLastOnline DEPRECATED
func (c *Commander) GetLastOnline(addr string) (string, error) {
	db, err := c.openDB(addr)
	if err != nil {
		return "0", err
	}
	defer closeDB(db)
	stmnt := "select date from messages where sender = ? order by id desc limit 1;"
	st, err := db.Prepare(stmnt)
	if err != nil {
		return "0", nil
	}
	defer st.Close()
	var date string
	err = st.QueryRow(addr).Scan(&date)
	if err != nil {
		return "0", nil
	}
	return date, nil
}

// GetLastMessage fetches very last message
func (c *Commander) GetLastMessage(addr string) (NewMessage, error) {
	var msg NewMessage
	db, err := c.openDB(addr)
	if err != nil {
		return NewMessage{}, err
	}
	defer closeDB(db)
	stmnt := `select
	id,
	origin,
	date,
	status,
	sender,
	input,
	pinned from messages where id = (select max(id) from messages);`
	st, err := db.Prepare(stmnt)
	if err != nil {
		return NewMessage{}, nil
	}
	defer st.Close()
	var id string
	var origin string
	var date string
	var status string
	var sender string
	var input string
	var pinned string
	err = st.QueryRow().Scan(&id, &origin, &date, &status, &sender, &input, &pinned)
	if err != nil {
		return NewMessage{}, nil
	}
	msg = NewMessage{id, origin, date, status, sender, input, pinned}
	return msg, nil
}

// GetLastMessageID fetches very last message's ID
func (c *Commander) GetLastMessageID(addr string) int {
	var id int
	db, err := c.openDB(addr)
	if err != nil {
		return 0
	}
	defer closeDB(db)
	stmnt := `select max(id) from messages;`
	st, err := db.Prepare(stmnt)
	if err != nil {
		return 0
	}
	defer st.Close()
	err = st.QueryRow().Scan(&id)
	if err != nil {
		return 0
	}
	return id
}

// GetNotifications fetches all unread messages
func (c *Commander) GetNotifications(addr string) []NewMessage {
	var messages []NewMessage
	db, err := c.openDB(addr)
	if err != nil {
		return []NewMessage{}
	}
	defer closeDB(db)
	stmnt := `select id, origin, date, status, sender, input, pinned from messages where status = 'new';`
	rows, err := db.Query(stmnt)
	if err != nil {
		return []NewMessage{}
	}
	defer rows.Close()
	for rows.Next() {
		var id string
		var origin string
		var date string
		var status string
		var sender string
		var input string
		var pinned string
		err = rows.Scan(&id, &origin, &date, &status, &sender, &input, &pinned)
		if err != nil {
			return []NewMessage{}
		}
		messages = append(messages, NewMessage{id, origin, date, status, sender, input, pinned})
	}
	err = rows.Err()
	if err != nil {
		return []NewMessage{}
	}
	return messages
}

// GetNewMessages fetches all messages with status == "unread"
func (c *Commander) GetNewMessages(addr string) (int, error) {
	amount := 0
	db, err := c.openDB(addr)
	if err != nil {
		return 0, err
	}
	defer closeDB(db)
	stmnt := `select id from messages where status = ? or status = ?;`
	rows, err := db.Query(stmnt, "self", "new")
	if err != nil {

		return 0, err
	}
	defer rows.Close()
	for rows.Next() {
		amount = amount + 1
	}
	err = rows.Err()
	if err != nil {
		return 0, err
	}
	return amount, nil
}

// GetMessageByID fetches message based on address & ID
func (c *Commander) GetMessageByID(addr string, id int) NewMessage {
	msg := NewMessage{}
	db, err := c.openDB(addr)
	if err != nil {
		return NewMessage{}
	}
	defer closeDB(db)
	stmnt := "select id, origin, date, status, sender, input, pinned from messages where id = ?;"
	st, err := db.Prepare(stmnt)
	if err != nil {
		return NewMessage{}
	}
	defer st.Close()
	var uid string
	var origin string
	var date string
	var status string
	var sender string
	var input string
	var pinned string
	err = st.QueryRow(id).Scan(&uid, &origin, &date, &status, &sender, &input, &pinned)
	if err != nil {
		return NewMessage{}
	}
	msg = NewMessage{uid, origin, date, status, sender, input, pinned}
	return msg
}

// GetMessagesByIDs fetches list of messages based on address and IDs
func (c *Commander) GetMessagesByIDs(addr string, ids string) []NewMessage {
	var msgs []NewMessage
	db, err := c.openDB(addr)
	if err != nil {
		return []NewMessage{}
	}
	defer closeDB(db)
	stmnt := fmt.Sprintf("select id, origin, date, status, sender, input, pinned from messages where id IN (%s);", ids)
	rows, err := db.Query(stmnt)
	if err != nil {
		return []NewMessage{}
	}
	defer rows.Close()
	for rows.Next() {
		var id string
		var origin string
		var date string
		var status string
		var sender string
		var input string
		var pinned string
		err = rows.Scan(&id, &origin, &date, &status, &sender, &input, &pinned)
		if err != nil {
			return []NewMessage{}
		}
		msgs = append(msgs, NewMessage{id, origin, date, status, sender, input, pinned})
	}
	err = rows.Err()
	if err != nil {
		return []NewMessage{}
	}
	return msgs
}

// UpdateSelfMessages updates message status
func (c *Commander) UpdateSelfMessages(address string) {
	db, err := c.openDB(address)
	if err != nil {
		return
	}
	defer closeDB(db)
	stmnt := `update messages set status = ? where status = ?;`
	db.Exec(stmnt, "down", "self")
	return
}

// UpdatedSelfNewMessages updates message status
func (c *Commander) UpdatedSelfNewMessages(address string) {
	db, err := c.openDB(address)
	if err != nil {
		return
	}
	defer closeDB(db)
	stmnt := `update messages set status = ? where status = ?;`
	db.Exec(stmnt, "self", "new")
	return
}

// UpdateSentMessages updates message status
func (c *Commander) UpdateSentMessages(address string) {
	db, err := c.openDB(address)
	if err != nil {
		return
	}
	defer closeDB(db)
	stmnt := `update messages set status = ? where status = ?;`
	db.Exec(stmnt, "read", "sent")
	return
}

// UpdateFailedMessage updates message status
func (c *Commander) UpdateFailedMessage(id int, address string) {
	db, err := c.openDB(address)
	if err != nil {
		return
	}
	defer closeDB(db)
	stmnt := `update messages set status = ? where id = ?;`
	db.Exec(stmnt, "failed", id)
	return
}

// UpdateUnfailMessage updates message status
func (c *Commander) UpdateUnfailMessage(id int, address string) {
	db, err := c.openDB(address)
	if err != nil {
		return
	}
	defer closeDB(db)
	stmnt := `update messages set status = ? where id = ?;`
	db.Exec(stmnt, "sent", id)
	return
}

// UpdateUnreadMessage updates message status
func (c *Commander) UpdateUnreadMessage(id int, address string) {
	db, err := c.openDB(address)
	if err != nil {
		return
	}
	defer closeDB(db)
	stmnt := `update messages set status = ? where id = ?;`
	db.Exec(stmnt, "unread", id)
	return
}

// SaveBlock inserts new blockchain block hash into
// table to use in the future as additional key
func (c *Commander) SaveBlock(hash string, number int) error {
	db, err := c.openDB("blocks")
	if err != nil {
		return err
	}
	stmnt := "select count(*) from blocks"
	st, err := db.Prepare(stmnt)
	if err != nil {
		return err
	}
	var amount int
	err = st.QueryRow().Scan(&amount)
	if err != nil {
		return err
	}
	st.Close()
	closeDB(db)
	if amount > 25 {
		return nil
	}
	db, err = c.openDB("blocks")
	if err != nil {
		return err
	}
	defer closeDB(db)
	stmnt = fmt.Sprintf(`insert into blocks(hash, number) values('%s', '%d')`, hash, number)
	_, err = db.Exec(stmnt)
	if err != nil {
		return err
	}
	return nil
}

// GetRandomBlockFromDB fetches random block hash to use as encryption key
func (c *Commander) GetRandomBlockFromDB() RandomBlock {
	db, err := c.openDB("blocks")
	if err != nil {
		return RandomBlock{}
	}
	defer closeDB(db)
	stmnt := "select id, hash, number from blocks where id >= (abs(random()) % (SELECT max(id) FROM blocks)) limit 1"
	st, err := db.Prepare(stmnt)
	if err != nil {
		return RandomBlock{}
	}
	var id int
	var hash string
	var number int
	err = st.QueryRow().Scan(&id, &hash, &number)
	if err != nil {
		return RandomBlock{}
	}
	st.Close()
	stmnt = fmt.Sprintf(`delete from blocks where id = '%d'`, id)
	_, err = db.Exec(stmnt)
	if err != nil {
		return RandomBlock{}
	}
	return RandomBlock{hash, number}
}

// AddNewUser saves user into database
func (c *Commander) AddNewUser(u *NewUser) error {
	fmt.Println(u.Addr)
	db, err := c.openDB(u.Addr)
	if err != nil {
		return err
	}
	stmnt := `create table messages(
	id integer not null primary key,
	origin text,
	date text,
	status text,
	sender text,
	input text,
	pinned boolean);`
	_, err = db.Exec(stmnt)
	fmt.Println(err)
	if err != nil {
		closeDB(db)
		return err
	}
	closeDB(db)
	db, err = c.openDB("history")
	if err != nil {
		pathNewUser := fmt.Sprintf("%s/history/%s.db", c.ConstantPath, u.Addr)
		os.Remove(pathNewUser)
		return err
	}
	stmnt = fmt.Sprintf(`insert into knownUsers(
		username,
		link,
		address,
		hash) values('', '%s', '%s', '%s');`, u.Link, u.Addr, u.Hash)
	_, err = db.Exec(stmnt)
	if err != nil {
		closeDB(db)
		pathNewUser := fmt.Sprintf("%s/history/%s.db", c.ConstantPath, u.Addr)
		os.Remove(pathNewUser)
		return err
	}
	closeDB(db)
	return nil
}

// SetUsername updates user's username from Onion Link to desired username
func (c *Commander) SetUsername(link string, username string) bool {
	db, err := c.openDB("history")
	if err != nil {
		return false
	}
	defer closeDB(db)
	stmnt := "update knownUsers set username = ? where link = ?"
	_, err = db.Exec(stmnt, username, link)
	if err != nil {
		return false
	}
	return true
}

// DeleteContact deletes contact from contact list
func (c *Commander) DeleteContact(link string) bool {
	db, err := c.openDB("history")
	if err != nil {
		return false
	}
	defer closeDB(db)
	address := c.GetAddressByLink(link)
	stmnt := fmt.Sprintf(`delete from knownUsers where link = '%s'`, link)
	_, err = db.Exec(stmnt)
	if err != nil {
		return false
	}
	path := c.ConstantPath
	fullPath := fmt.Sprintf("%s/history/%s.db", path, address)
	os.Remove(fullPath)
	return true
}

// GetPinnedMessage fetches pinned message
func (c *Commander) GetPinnedMessage(addr string) int {
	db, err := c.openDB(addr)
	if err != nil {
		return 0
	}
	defer closeDB(db)
	stmnt := "select id from messages where pinned = ?"
	st, err := db.Prepare(stmnt)
	if err != nil {
		return 0
	}
	defer st.Close()
	var mid int
	err = st.QueryRow("true").Scan(&mid)
	if err != nil {
		return 0
	}
	return mid
}

// PinMessage sets message's status to pinned
// if one message was pinned before, resets it's status
func (c *Commander) PinMessage(addr string, mid int) {
	db, err := c.openDB(addr)
	if err != nil {
		return
	}
	defer closeDB(db)
	stmnt := "select id from messages where pinned = ?"
	st, err := db.Prepare(stmnt)
	if err != nil {
		return
	}
	defer st.Close()
	id := 0
	err = st.QueryRow("true").Scan(&id)
	if err != nil {
		stmnt = "update messages set pinned = ? where id = ?"
		_, err = db.Exec(stmnt, "true", mid)
		if err != nil {

		}
		return
	}
	if id != 0 {
		c.UnpinMessage(addr)
	}
	stmnt = "update messages set pinned = ? where id = ?"
	_, err = db.Exec(stmnt, "true", mid)
	if err != nil {
		return
	}
	return
}

// UnpinMessage opposite to PinMessage
func (c *Commander) UnpinMessage(addr string) {
	db, err := c.openDB(addr)
	if err != nil {
		return
	}
	defer closeDB(db)
	stmnt := "update messages set pinned = ?"
	db.Exec(stmnt, "false")
	return
}

// SaveMessage inserts message to table
func (c *Commander) SaveMessage(addr string, rec string, mtype string, msg string) int {
	status := "sent"
	db, err := c.openDB(rec)
	if err != nil {
		return 0
	}
	defer closeDB(db)
	if addr == rec {
		status = "new"
	}
	date := strconv.Itoa(int(time.Now().Unix()))
	stmnt := fmt.Sprintf(
		`insert into messages(
		origin,
		date,
		status,
		sender,
		input,
		pinned) values(
		'%s',
		'%s',
		'%s',
		'%s',
		'%s',
		'false');`, mtype, date, status, addr, msg)
	fmt.Println(stmnt)
	_, err = db.Exec(stmnt)
	fmt.Println(err)
	if err != nil {
		return 0
	}
	id := c.GetLastMessageID(rec)
	return id
}
