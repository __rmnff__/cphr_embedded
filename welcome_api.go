package api

// WelcomeChat generates default chats that are shown to unsubscribed users
func (c *Commander) WelcomeChat() string {
	var chat = User{
		Username: "Cipher Dev Team",
		Link:     "nolink.onion",
		Addr:     "0x9De9223eb770E377ab148B8d37Fee348E8D691bC",
		LastMessage: NewMessage{
			ID:     "0",
			Type:   "text",
			Date:   "1607181907",
			Status: "read",
			Sender: "0x9De9223eb770E377ab148B8d37Fee348E8D691bC",
			Text:   "Hey, @username and welcome to Cipher Community! Enjoy all the benefits of truly private messaging and feel free to share any personal information with anyone inside of our server-less architecture! You’re now one step away to start. Just activate your subscription plan and dig into unlimited private conversations!\n\nWe are here to help, please forward all the queries to the email below ⬇️ customer.cipher@protonmail.com",
			Pinned: "false",
		},
		NewMessages:   "0",
		Notifications: nil,
		LastOnline:    "0",
	}
	var chats []User
	chats = append(chats, chat)
	return formResponse(chats, "")
}

// WelcomeMessage generates default message that is shown to unsubscribed users
func (c *Commander) WelcomeMessage() string {
	var message = NewMessage{
		ID:     "0",
		Type:   "text",
		Date:   "1607181907",
		Status: "read",
		Sender: "0x9De9223eb770E377ab148B8d37Fee348E8D691bC",
		Text:   "Hey, @username and welcome to Cipher Community! Enjoy all the benefits of truly private messaging and feel free to share any personal information with anyone inside of our server-less architecture! You’re now one step away to start. Just activate your subscription plan and dig into unlimited private conversations!\n\nWe are here to help, please forward all the queries to the email below ⬇️ customer.cipher@protonmail.com",
		Pinned: "false",
	}
	var msgs []NewMessage
	msgs = append(msgs, message)
	return formResponse(msgs, "")
}
