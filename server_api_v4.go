package api

import (
	"bufio"
	"net"
	"net/http"
)

// RunServers allows Commander to run both HTTP and TCP servers
// Whenever TCP connection ends, new is opened and reassigned in Commander
func (c *Commander) RunServers() {
	ln, _ := net.Listen("tcp", c.TCPPort)
	conn, _ := ln.Accept()
	c.SetTCPConnection(conn)
	server := &http.Server{
		Addr: c.HTTPPort,
		Handler: http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			response := []byte(DefaultHandlerHTTP(r.URL.Query(), c))
			c.TCPConnection.Write(response)
			w.Write(response)
		}),
	}
	go func() {
		for {
			message, _ := bufio.NewReader(conn).ReadString('\n')
			if message == "\n" {
				conn.Close()
				conn, _ := ln.Accept()
				c.SetTCPConnection(conn)
			}
			response := c.handleMessage([]byte(message))
			conn.Write([]byte(response + "\n"))
		}
	}()
	server.ListenAndServe()
}
