package api

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
	"time"
)

// DefaultError defines default response to unhandled errors
const DefaultError = `{"res": "", "error": "can't convert struct to JSON"}`

// DefaultHandlerHTTP handles HTTP requests based on GET handler /?call=<request>
var DefaultHandlerHTTP = func(request map[string][]string, c *Commander) string {
	switch strings.Join(request["call"], "") {
	case "send":
		return c.SendMessageWithType(request, "text")
	case "sysSend":
		return c.SendMessageWithType(request, "system")
	case "fileSend":
		return c.SendMessageWithType(request, "file")
	case "imageSend":
		return c.SendMessageWithType(request, "image")
	case "audioSend":
		return c.SendMessageWithType(request, "audio")
	case "resend":
		return c.SendMessageWithType(request, "")
	case "notify":
		return c.ProcessNotification(request)
	case "greeting":
		return c.InitiateGreeting(request)
	case "greetingOk":
		return c.ProcessGreeting(request)
	case "chats":
		return c.FormChats()
	case "inbox":
		return c.ProcessInbox(request)
	case "inboxFull":
		return c.ProcessFullInbox(request)
	case "inboxFresh":
		return c.ProcessFreshInbox(request)
	case "inboxFired":
		return c.ProcessInboxFired(request)
	case "updatedMessages":
		return c.UpdatedMessage(request)
	default:
		for k, v := range request {
			s := fmt.Sprintf("%s: %v", k, v)
			fmt.Printf("%s\n", s)
		}
		return formResponse("", "unrecognized HTTP call")
	}
}

// SendMessageWithType compiles messages, encrypts and sends 'em via TOR-Network
func (c *Commander) SendMessageWithType(request map[string][]string, t string) string {
	var msg string
	var id int
	rec := strings.Join(request["recepient"], "")
	mid := strings.Join(request["id"], "")
	link := c.GetHSLink()
	if mid == "" {
		msg = strings.Join(request["msg"], "")
		a := c.GetSelfAddress()
		id = c.SaveMessage(a, rec, t, msg)
		if id == 0 {
			return formResponseHTTP("", "can't save message")
		}
	} else {
		id, _ = strconv.Atoi(mid)
		addr := strings.Join(request["address"], "")
		rec = addr
		message := c.GetMessageByID(addr, id)
		msg = message.Text
		t = message.Type
		c.UpdateUnfailMessage(id, addr)
	}
	cb := c.GetLinkByAddress(rec)
	if cb == "" {
		return formResponseHTTP("", "transaction didn't happen, try resending the message")
	}
	emsg := c.CipherMessage(rec, msg)
	tx, err := FormRawTxWithBlockchain(emsg, rec)
	if err != nil {
		return formResponseHTTP("", "can't form transaction")
	}
	go func() {
		res := &ResponseJSON{}
		uri := fmt.Sprintf("%s/?call=notify&callback=%s&tx=%s&type=%s", cb, link, tx, t)
		r, err := Request(uri)
		errParsing := json.Unmarshal([]byte(r), res)
		if err != nil || errParsing != nil || res.Res != "ok" {
			if err != nil {
				fmt.Printf("%s\n", err.Error())
			}
			if errParsing != nil {
				fmt.Printf("%s\n", errParsing.Error())
			}
			c.TCPConnection.Write([]byte("message failed to be delivered"))
			c.UpdateFailedMessage(id, rec)
		}
	}()
	return formResponseHTTP(tx, "")
}

// ProcessNotification handles notifications
func (c *Commander) ProcessNotification(request map[string][]string) string {
	var t string
	var address string
	cb := strings.Join(request["callback"], "")
	if t = strings.Join(request["type"], ""); t == "" {
		t = "text"
	}
	if address = c.GetAddressByLink(cb); address == "" {
		return formResponseHTTP("", "no such user found")
	}
	tx := strings.Join(request["tx"], "")
	trimmedTx := strings.Split(tx, "x")[1]
	decodedTx, err := DecodeRawTx(trimmedTx)
	if err != nil {
		return formResponseHTTP("", "can't decode tx")
	}
	m := string(c.DecipherMessage(address, decodedTx))
	if saved := c.SaveMessage(address, address, t, m); saved == 0 {
		return formResponseHTTP("", "can't save message")
	}
	go func(c *Commander, address string) {
		time.Sleep(time.Second * 5)
		c.UpdatedSelfNewMessages(address)
	}(c, address)
	latest, _ := c.GetLastMessage(address)
	fmted, _ := json.Marshal(latest)
	c.TCPConnection.Write([]byte(fmted))
	return formResponseHTTP("ok", "")
}

// InitiateGreeting handles cipher exchange at the first connection
func (c *Commander) InitiateGreeting(request map[string][]string) string {
	callback := strings.Join(request["callback"], "")
	cb := fmt.Sprintf("%s.onion", callback)
	if existance := c.CheckExistance(cb); existance != false {
		return formResponseHTTP("", "already connected")
	}
	cipher := GenRandomString(32)
	hexedCipher := Hexify(cipher)
	link := strings.Split(c.GetHSLink(), ".")[0]
	selfAddr := c.GetSelfAddress()
	formattedURL := fmt.Sprintf(`%s/?call=greetingOk&callback=%s&address=%s&cipher=%s`, cb, link, selfAddr, hexedCipher)
	response, err := Request(formattedURL)
	if err != nil {
		return formResponseHTTP("", "request failed to reach recepient")
	}
	r := &ResponseJSON{}
	if err = json.Unmarshal([]byte(response), r); err != nil {
		return formResponseHTTP("", "can't parse response")
	}
	if str, ok := r.Res.(string); ok {
		if err = c.AddNewUser(&NewUser{cb, str, hexedCipher}); err != nil {
			return formResponseHTTP("", "can't save user")
		}
		return formResponseHTTP("ok", "")
	}
	return formResponseHTTP("", "can't save user")
}

// ProcessGreeting required by InitiateGreeting on the receiver's side
func (c *Commander) ProcessGreeting(request map[string][]string) string {
	addr := strings.Join(request["address"], "")
	callback := strings.Join(request["callback"], "")
	cipher := strings.Join(request["cipher"], "")
	cb := fmt.Sprintf("%s.onion", callback)
	newUser := &NewUser{
		cb,
		addr,
		cipher,
	}
	if err := c.AddNewUser(newUser); err != nil {
		return formResponseHTTP("", "can't save user")
	}
	return formResponseHTTP(c.GetSelfAddress(), "")
}

func formResponseHTTP(response interface{}, err string) string {
	if err != "" {
		responseStruct := ResponseJSON{"", err}
		responseStructStringified, e := json.Marshal(responseStruct)
		if e != nil {
			responseStruct = ResponseJSON{"", err}
			responseStructStringified, e = json.Marshal(responseStruct)
			if e != nil {
				responseStructStringified = []byte(DefaultError)
			}
		}
		return string(responseStructStringified)
	}
	responseStruct := ResponseJSON{response, ""}
	responseStructStringified, e := json.Marshal(responseStruct)
	if e != nil {
		responseStruct = ResponseJSON{"", err}
		responseStructStringified, e = json.Marshal(responseStruct)
		if e != nil {
			responseStructStringified = []byte(DefaultError)
		}
	}
	return string(responseStructStringified)
}

// FormChats returns all chats from database
func (c *Commander) FormChats() string {
	chats := c.GetChats()
	if len(chats) <= 0 {
		return formResponse([]string{}, "no chats found")
	}
	for i := range chats {
		_, err := json.Marshal(chats[i])
		if err != nil {
			formResponse([]string{}, "chats are corrupted, you have to reinstall the application")
		}
	}
	return formResponse(chats, "")
}

// ProcessFullInbox returns all messages of the desired conversation
func (c *Commander) ProcessFullInbox(request map[string][]string) string {
	var msgs []NewMessage
	var err error
	address := strings.Join(request["address"], "")
	if msgs, err = c.GetFullChatHistory(address); err != nil || len(msgs) == 0 {
		return formResponse([]string{}, "can't get messages")
	}
	for i := range msgs {
		if _, err = json.Marshal(msgs[i]); err != nil {
			return formResponse([]string{}, "can't get messages")
		}
	}
	go func() {
		l := c.GetLinkByAddress(address)
		a := c.GetSelfAddress()
		uri := fmt.Sprintf("%s/?call=inboxFired&address=%s", l, a)
		Request(uri)
	}()
	return formResponse(msgs, "")
}

// ProcessInbox returns the latest n-amount of messages of the desired conversation
func (c *Commander) ProcessInbox(request map[string][]string) string {
	var msgs []NewMessage
	var err error
	address := strings.Join(request["address"], "")
	if msgs, err = c.GetChatHistory(address); err != nil || len(msgs) == 0 {
		return formResponse([]string{}, "can't get messages")
	}
	for i := range msgs {
		if _, err = json.Marshal(msgs[i]); err != nil {
			return formResponse([]string{}, "can't get messages")
		}
	}
	go func() {
		l := c.GetLinkByAddress(address)
		a := c.GetSelfAddress()
		uri := fmt.Sprintf("%s/?call=inboxFired&address=%s", l, a)
		Request(uri)
	}()
	return formResponse(msgs, "")
}

// ProcessFreshInbox only rreturns new messages (BROKEN)
func (c *Commander) ProcessFreshInbox(request map[string][]string) string {
	var msgs []NewMessage
	var err error
	address := strings.Join(request["address"], "")
	if msgs, err = c.GetLatestMessages(address); err != nil || len(msgs) == 0 {
		return formResponse([]string{}, "can't get messages")
	}
	for i := range msgs {
		if _, err = json.Marshal(msgs[i]); err != nil {
			return formResponse([]string{}, "can't parse message")
		}
	}
	go func() {
		l := c.GetLinkByAddress(address)
		a := c.GetSelfAddress()
		uri := fmt.Sprintf("%s/?call=inboxFired&address=%s", l, a)
		Request(uri)
	}()
	return formResponse(msgs, "")
}

// ProcessInboxFired updates sent messages status to "sent"
func (c *Commander) ProcessInboxFired(request map[string][]string) string {
	address := strings.Join(request["address"], "")
	c.UpdateSentMessages(address)
	return formResponse("ok", "")
}

// UpdatedMessage gets messages by IDs
func (c *Commander) UpdatedMessage(request map[string][]string) string {
	var err error
	address := strings.Join(request["address"], "")
	ids := strings.Join(request["ids"], "")
	msgs := c.GetMessagesByIDs(address, ids)
	for i := range msgs {
		if _, err = json.Marshal(msgs[i]); err != nil {
			return formResponse([]string{}, "can't parse message")
		}
	}
	return formResponse(msgs, "")
}
