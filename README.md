# Cipher Golang core

[![N|Solid](https://i.imgur.com/6DW6ZUe.png)](https://apple.co/35gwr6F)

![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)

Cipher is a serverless messenger based on TOR-Network, IPFS and Ethereum Blockchain.

  - Onion Services as "servers" running directly on mobile devices
  - IPFS for file exchange
  - Ethereum Blockchain transactions as additional privacy level

# New Features!

  - File exchange! IPFS allows users to exchange files using content's hash as a link
  - User namings! Click user's avatar and set a name for a chat


You can also:
  - Use TOR as a VPN solution
  - Chat with people all over the world
  - Save documents and images received from other users

### Tech

Cipher uses a number of open source projects to work properly:
  - [go-ethereum](https://github.com/ethereum/go-ethereum)
  - [lz4](github.com/pierrec/lz4)
  - [go-sqlite3](github.com/mattn/go-sqlite3)
  - [go-ipfs-api](github.com/ipfs/go-ipfs-api)
  - [go-qidenticon](github.com/jakobvarmose/go-qidenticon)
  - [go-qrcode](github.com/skip2/go-qrcode)

### Development

Want to contribute? Great!

Contact us via [email](customer.cipher@protonmail.com) to join the team.

#### Building from source

For desktop CLI soultion:
```go
package main

import (
	"fmt"

	cphr "github.com/cphr_embedded"
)

func main() {
	cmdr := cphr.NewCommander("/current/directory/path")
	updated := cmdr.UpdateStorage()
	fmt.Printf("Database updated? %v\n", updated)
	cmdr.ConfigureTorrc()
	fmt.Println("torrc configured")
	cmdr.RunRealServer()
}
```

For iOS release:
```sh
# in order to build the app, replace bind/hello project with cipher source and run
$ gomobile bind -target=ios golang.org/x/mobile/example/bind/hello
```

### Todos

 - Replace internal HTTP-polling with Sockets
 - Enhance current SQL-solution

License
----

MIT


**Free Software, Hell Yeah!**
