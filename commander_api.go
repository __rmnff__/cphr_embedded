package api

import (
	"database/sql"
	"fmt"
	"net"
)

// Commander represents the universal structure, that allows users to
// fire public functions and handle socket/http/TOR servers
type Commander struct {
	ConstantPath      string
	Connection        net.Conn
	DbFilename        string
	DbConnection      *sql.DB
	MessagesCount     int
	OldMessagesCount  int
	OldMessagesObsrvr bool
	TCPConnection     net.Conn
	HTTPPort          string
	HTTPTLSPort       string
	TCPPort           string
}

// NewCommander returns Commander structure with defined path
func NewCommander(path string) *Commander {
	var connection net.Conn
	var dbconnection sql.DB
	var dbfilename string
	var messagecount int
	var HTTPPort string = ":4887"
	var HTTPTLSPort string = ":4888"
	var TCPPort string = ":4889"
	observer := false
	return &Commander{
		ConstantPath:      path,
		Connection:        connection,
		DbFilename:        dbfilename,
		DbConnection:      &dbconnection,
		MessagesCount:     messagecount,
		OldMessagesCount:  messagecount,
		OldMessagesObsrvr: observer,
		TCPConnection:     connection,
		HTTPPort:          HTTPPort,
		HTTPTLSPort:       HTTPTLSPort,
		TCPPort:           TCPPort,
	}
}

// ChangeCommanderPath no explanation required
func (c *Commander) ChangeCommanderPath(path string) {
	c.ConstantPath = path
}

// AcceptConnection no explanation required
func (c *Commander) AcceptConnection(connection net.Conn) {
	c.Connection = connection
}

// SetDatabaseConnection no explanation required
func (c *Commander) SetDatabaseConnection(name string, conn *sql.DB) {
	c.DbFilename = name
	c.DbConnection = conn
}

// SetMessageCount no explanation required
func (c *Commander) SetMessageCount(count int) {
	c.MessagesCount = count
}

// SetOldMessageCount no explanation required
func (c *Commander) SetOldMessageCount(count int) {
	c.OldMessagesCount = count
}

// StartObserving no explanation required
func (c *Commander) StartObserving() {
	c.OldMessagesObsrvr = true
}

// StopObserving no explanation required
func (c *Commander) StopObserving() {
	c.OldMessagesObsrvr = false
}

// SetTCPConnection no explanation required
func (c *Commander) SetTCPConnection(conn net.Conn) {
	c.TCPConnection = conn
}

// ChangeHTTPPort no explanation required
func (c *Commander) ChangeHTTPPort(port int) {
	fixed := fmt.Sprintf(":%d", port)
	fixedTLS := fmt.Sprintf(":%d", port+1)
	c.HTTPPort = fixed
	c.HTTPTLSPort = fixedTLS
}

// ChangeTCPPort no explanation required
func (c *Commander) ChangeTCPPort(port int) {
	fixed := fmt.Sprintf(":%d", port)
	c.TCPPort = fixed
}
