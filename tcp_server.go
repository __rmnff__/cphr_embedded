package api

import (
	"encoding/json"
)

// RequestJSON defines TCP's request structure
type RequestJSON struct {
	Req  string      `json:"req"`
	Data interface{} `json:"data"`
}

// ResponseJSON defines TCP's response structure
type ResponseJSON struct {
	Res interface{} `json:"res"`
	Err string      `json:"err"`
}

// DefaultHandlerTCP handles TCP requests based on JSON structures
var DefaultHandlerTCP = func(req string, data map[string]interface{}, c *Commander) string {
	switch req {
	case "ping":
		return formResponse("pong", "")
	case "id":
		return formResponse(c.GetHSLink(), "")
	default:
		return formResponse("", "unrecognized TCP call")
	}
}

func (c *Commander) handleMessage(message []byte) string {
	var request RequestJSON
	if err := json.Unmarshal(message, &request); err != nil {
		return formResponse("", "Can't handle request, retry")
	}
	return DefaultHandlerTCP(request.Req, request.Data.(map[string]interface{}), c)
}

func formResponse(response interface{}, err string) string {
	if err != "" {
		responseStruct := ResponseJSON{"", err}
		responseStructStringified, e := json.Marshal(responseStruct)
		if e != nil {
			responseStruct = ResponseJSON{"", err}
			responseStructStringified, e = json.Marshal(responseStruct)
			if e != nil {
				responseStructStringified = []byte("")
			}
		}
		return string(responseStructStringified)
	}
	responseStruct := ResponseJSON{response, ""}
	responseStructStringified, e := json.Marshal(responseStruct)
	if e != nil {
		responseStruct = ResponseJSON{"", err}
		responseStructStringified, e = json.Marshal(responseStruct)
		if e != nil {
			responseStructStringified = []byte("")
		}
	}
	return string(responseStructStringified)
}
