package api

import (
	"io/ioutil"
	"path/filepath"
	"strings"
)

// PostFile returns file content as byte slice array
func (c *Commander) PostFile(fpath string) []byte {
	content, err := ioutil.ReadFile(fpath)
	if err != nil {
		return []byte{}
	}
	return content
}

// PostFilename returns filename from pathway
func (c *Commander) PostFilename(fpath string) string {
	return filepath.Base(fpath)
}

// GetFile returns pathway to file
func (c *Commander) GetFile(decoded string) []byte {
	split := strings.Split(decoded, ":")
	stringified := split[1]
	return []byte(stringified)
}

// GetFilename returns filename from pathway
func (c *Commander) GetFilename(decoded string) string {
	split := strings.Split(decoded, ":")
	filename := split[0]
	return filename
}
